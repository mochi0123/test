﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectTest.Models;
using System.Configuration;
using System.Data.SqlClient;


namespace ProjectTest.Controllers
{
    //public class UsersController : Controller

   
    //{
    //    SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyWeb5"].ConnectionString);
    //    SqlDataAdapter adp = new SqlDataAdapter();
    //    SqlCommand cmd = new SqlCommand();


    //    private Web5Entities db = new Web5Entities();

    //    // GET: Users
    //    public ActionResult Index()
    //    {
    //        var user = db.User.Include(u => u.StoreStatus);
    //        return View(user.ToList());
    //    }

    //    // GET: Users/Details/5
    //    public ActionResult Details(string id)
    //    {
    //        if (id == null)
    //        {
    //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
    //        }
    //        User user = db.User.Find(id);
    //        if (user == null)
    //        {
    //            return HttpNotFound();
    //        }
    //        return View(user);
    //    }

    //    // GET: Users/Edit/5
    //    public ActionResult Edit(string id)
    //    {
    //        if (id == null)
    //        {
    //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
    //        }
    //        User user = db.User.Find(id);
    //        if (user == null)
    //        {
    //            return HttpNotFound();
    //        }
    //        ViewBag.User_StoreStatusNo = new SelectList(db.StoreStatus, "StoreS_No", "StoreS_Status", user.User_StoreStatusNo);
    //        return View(user);
    //    }

 //       // POST: Users/Edit/5
 //       // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
 //       // 詳細資訊，請參閱 https://go.microsoft.com/fwlink/?LinkId=317598。
 //       [HttpPost]
 //       [ValidateAntiForgeryToken]
 //       public ActionResult Edit([Bind(Include = "User_ID,User_Account,User_Password,User_BuyerName,User_StoreName,User_TrueName,User_Email,User_Phone,User_Gender,User_Birth,User_Address,User_Photo")] User user,User u,string pic)
 //       {

 //           //確認是否為登入狀態!!
 //           if (Session["User_ID"] != null)
 //           {
 //               cmd.Parameters.AddWithValue("@id", Session["User_ID"]);
 //               adp.SelectCommand = cmd;
 //           }
 //           else
 //           {
 //               return RedirectToAction("Login", "WebHome");
 //           }


 //           //未填寫的資料行預設
 //           u.User_StoreName = u.User_BuyerName;
 //           u.User_Photo = pic;
 //           u.User_StoreCoverPic = u.User_StoreCoverPic == null ? " " : u.User_StoreCoverPic;
 //           u.User_Address = u.User_Address==null? " " : u.User_Address;
 //           u.User_StoreIntroduction = u.User_StoreIntroduction == null ? " " : u.User_StoreIntroduction;
 //           u.User_StoreProductDisplay = "";
 //           u.User_FbID = "";
 //           u.User_Status = u.User_Status == null ? " " : u.User_Status;
 //           u.User_StoreStatusNo = u.User_StoreStatusNo == null ? " " : u.User_StoreStatusNo;
 //           u.User_Birth = u.User_Birth==null ? DateTime.Now :u.User_Birth;


 //           //確認生日是否有超過當天日期
 //           if (u.User_Birth >= DateTime.Now)
 //           {
 //               ViewBag.checkbirth = "生日輸入錯誤!";
 //               //資料輸入錯誤，回到頁面
 //               return View(u);
 //           }

 //           //是否通過資料驗證
 //           if (ModelState.IsValid)
 //           {
 //               db.Entry(u).State = EntityState.Modified;
 //               try
 //               {
 //                   db.SaveChanges();
 //               }
 //               catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
 //               {
 //                   //sql server 報錯，通常是UQ的重複問題
 //                   //ViewBag.error += "輸入資料已註冊，請修改!!";
 //                   //資料輸入錯誤，回到註冊頁面
 //                   //return View(u);
 //               }
 //               catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
 //               {
 //                   //sql server 報錯，通常是datatype跟nou null問題
 //                   Exception raise = dbEx;
 //                   foreach (var validationErrors in dbEx.EntityValidationErrors)
 //                   {
 //                       foreach (var validationError in validationErrors.ValidationErrors)
 //                       {
 //                           string message = string.Format("{0}:{1}",
 //                               validationErrors.Entry.Entity.ToString(),
 //                               validationError.ErrorMessage);
 //                           // raise a new exception nesting
 //                           // the current instance as InnerException
 //                           raise = new InvalidOperationException(message, raise);
 //                       }
 //                   }
 //                   throw raise;
 //               }
 //               return RedirectToAction("Index");
 //           }



           
 //           return View(user);
 //       }

 ////儲存上傳的大頭照

 //       public string EditPic()
 //       {
 //           var pic = Request.Files["pic"];
 //           var file_name = "";
 //           if (pic != null)
 //           {
 //               if (pic.ContentLength > 0)
 //               {
 //                   file_name = DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "").Replace("上午", "").Replace("下午", "") + ".jpg";

 //                   pic.SaveAs(Server.MapPath("~/photo/user_pic/" + file_name));

                   
                 
 //               }
 //           }


 //           return file_name;
 //       }

 //       //儲存大頭照的修改
 //       public JsonResult EditSave(string user_pic)
 //       {



 //           string sql = "update [User] set  User_Photo=@pic" +
 //              " where[User_ID] =@id";

 //           cmd.CommandText = sql;
 //           cmd.Connection = Conn;
        
 //           cmd.Parameters.AddWithValue("@pic", user_pic);
 //           cmd.Parameters.AddWithValue("@id", Session["User_ID"]);



 //           Conn.Open();
 //           cmd.ExecuteNonQuery();
 //           Conn.Close();

 //           return Json(new { user_pic = user_pic });
 //       }



 //       protected override void Dispose(bool disposing)
 //       {
 //           if (disposing)
 //           {
 //               db.Dispose();
 //           }
 //           base.Dispose(disposing);
 //       }
 //   }
}
