﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectTest.Controllers
{
    public class StoreProfileController : Controller
    {
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyWeb5"].ConnectionString);
        SqlDataAdapter adp = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();

        
        // GET: StoreProfile
        public ActionResult Index()
        {
            return View();
        }

        //賣場設定頁面
        public ActionResult Edit()
        {
            string sql = "select u.*,ss.StoreS_Status from [User] as u inner join [StoreStatus] as ss on u.User_StoreStatusNo=ss.StoreS_No where u.User_ID=@id";

            SqlCommand cmd = new SqlCommand(sql,Conn);

            //檢查登入狀態，訪客身分會連到登入頁面
            if (Session["User_ID"] != null)
            {
                cmd.Parameters.AddWithValue("@id", Session["User_ID"]);
                adp.SelectCommand = cmd;
            }
            else
            {
                return RedirectToAction("Login","WebHome");
            }
            DataSet ds = new DataSet();
            adp.Fill(ds);

            return View(ds.Tables[0]);
        }

        //儲存上傳的新封面圖
        public string EditCover() {
            var cover = Request.Files["cover"];
            var file_name="";
            if (cover != null)
            {
                if (cover.ContentLength > 0)
                {
                    file_name = DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "").Replace("上午", "").Replace("下午", "") + ".jpg";

                    cover.SaveAs(Server.MapPath("~/photo/store_profile/" + file_name));
                }
            }

            return file_name;
        }

        //儲存賣場介紹的修改
        public JsonResult EditSave(string name,string intro,string status,string cover_pic) {
;           
            string sql = "update [User] set User_StoreName=@name, User_StoreIntroduction=@intro,"+
                " User_StoreStatusNo=@status, User_StoreCoverPic=@cover"+
                " where[User_ID] =@id";
            cmd.CommandText = sql;
            cmd.Connection = Conn;
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@intro", intro);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@cover", cover_pic);
            cmd.Parameters.AddWithValue("@id", Session["User_ID"]);

            Conn.Open();
            cmd.ExecuteNonQuery();
            Conn.Close();

            return Json(new { name = name, intro = intro, status = status, cover_pic  = cover_pic });
        }
    }
}