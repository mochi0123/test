﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;  
using System.Data.SqlClient; 
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Windows.Forms;

namespace ProjectTest.Controllers
{
    public class UserProfileController : Controller
    {
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyWeb5"].ConnectionString);

        SqlCommand Cmd = new SqlCommand();

        SqlDataAdapter adp = new SqlDataAdapter();  //配合05-2-7方法:建立一般方法querySql()

        private void executeSql(string sql)  //要增刪修都要呼叫這個函數
        {
            Cmd.CommandText = sql;
            Cmd.Connection = Conn;

            Conn.Open();
            Cmd.ExecuteNonQuery();

            Conn.Close();
        }

        //05-2-7 建立一般方法querySql()-可傳入SQL字串並傳回DataTable物件
        private DataTable querySql(string sql)
        {
            Cmd.CommandText = sql;
            Cmd.Connection = Conn;
            adp.SelectCommand = Cmd;

            DataSet ds = new DataSet();
            adp.Fill(ds);

            return ds.Tables[0];
        }

        // GET: UserProfile
        public ActionResult Index()
        {

            string sql = "select User_ID,User_Account,User_Email from [User]";



            SqlDataAdapter adp = new SqlDataAdapter(sql, Conn);
            DataSet ds = new DataSet();
            adp.Fill(ds);

            DataTable dt = ds.Tables[0];

            return View(dt);

        }


        public ActionResult Edit(string id)
        {
            string sql = "select User_Account,User_BuyerName,User_TrueName,User_Email,User_Phone,User_Birth,User_Address from [User]  where User_ID=@User_ID";
            Cmd.Parameters.AddWithValue("@User_ID", id);
            DataTable dt = querySql(sql);
            return View(dt);
        }


        [HttpPost]
        public ActionResult Edit(string User_Account, string User_BuyerName, string User_TrueName, string User_Email, string User_Phone, DateTime User_Birth, string User_Address)
        {
            string sql = "update [User] set User_Account=@User_Account,User_BuyerName=@User_BuyerName,User_TrueName=@User_TrueName,User_Email=@User_Email,User_Phone=@User_Phone,User_Birth=@User_Birth,User_Address=@User_Address  where User_ID=@User_ID";  //這裡要取參數@，通常會取的跟Action代的參數名稱(資料表欄位)一樣

            SqlCommand Cmd = new SqlCommand(sql, Conn);


            Cmd.Parameters.AddWithValue("@User_Account", User_Account);
            //Cmd.Parameters.AddWithValue("@User_Password", User_Password);
            Cmd.Parameters.AddWithValue("@User_BuyerName", User_BuyerName);
            Cmd.Parameters.AddWithValue("@User_TrueName", User_TrueName);
            Cmd.Parameters.AddWithValue("@User_Email", User_Email);
            Cmd.Parameters.AddWithValue("@User_Phone", User_Phone);
            // Cmd.Parameters.AddWithValue("@User_Gender", User_Gender);
            Cmd.Parameters.AddWithValue("@User_Birth", User_Birth);
            //Cmd.Parameters.AddWithValue("@User_Photo", User_Photo);
            Cmd.Parameters.AddWithValue("@User_Address", User_Address);
            Cmd.Parameters.AddWithValue("@User_ID", Session["User_ID"]);

            Conn.Open();
            Cmd.ExecuteNonQuery();
            Conn.Close();

            Session["User_BuyerName"] = User_BuyerName;

            return Redirect(Request.UrlReferrer.ToString());
            //return RedirectToAction("Edit", "UserProfile");

        }


        //修改密碼
        public ActionResult EditPwd(string id)
        {
            string sql = "select User_Password from [User]  where User_ID=@User_ID";
            Cmd.Parameters.AddWithValue("@User_ID", id);
            DataTable dt = querySql(sql);
            return View(dt);
        }


       //修改密碼
        public JsonResult SaveNewPwd(string User_Password)
        {
            string sql = "update [User] set User_Password=@User_Password  where User_ID=@User_ID";

            SqlCommand Cmd = new SqlCommand(sql, Conn);

            Cmd.Parameters.AddWithValue("@User_Password", User_Password);

            Cmd.Parameters.AddWithValue("@User_ID", Session["User_ID"]);

            Conn.Open();
            Cmd.ExecuteNonQuery();
            Conn.Close();

            return Json(new { User_Password = User_Password });

        }


        ////修改大頭照
        //public ActionResult EditPic(string id)
        //{
        //    string sql = "select User_Photo from [User]  where User_ID=@User_ID";
        //    Cmd.Parameters.AddWithValue("@User_ID", id);
        //    DataTable dt = querySql(sql);
        //    return View(dt);
        //}


        //[HttpPost]
        //public ActionResult SavePic(string User_Photo)
        //{
        //    string sql = "update [User] set User_Photo=@User_Photo  where User_ID=@User_ID";  //這裡要取參數@，通常會取的跟Action代的參數名稱(資料表欄位)一樣

        //    SqlCommand Cmd = new SqlCommand(sql, Conn);



        //    Cmd.Parameters.AddWithValue("@User_Photo", User_Photo);

        //    Cmd.Parameters.AddWithValue("@User_ID", Session["User_ID"]);

        //    Conn.Open();
        //    Cmd.ExecuteNonQuery();
        //    Conn.Close();



        //    return Redirect(Request.UrlReferrer.ToString());
        //    //return RedirectToAction("Edit", "UserProfile");

        //}


        ////儲存上傳的新封面圖
        //public string SavePic()
        //{
        //    var pic = Request.Files["pic"];
        //    var file_name = "";
        //    if (pic != null)
        //    {
        //        if (pic.ContentLength > 0)
        //        {
        //            file_name = DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "").Replace("上午", "").Replace("下午", "") + ".jpg";

        //            pic.SaveAs(Server.MapPath("~/photo/user_pic/" + file_name));
        //        }
        //    }

        //    return file_name;
        //}


        //public void button1_Click(object sender, EventArgs e)
        //{
        //    var dirname =

        //    bool dir = false;
        //    dir = Directory.Exists(@"D:\webapp\WebAPP\02Controller\Photos\rat");
        //    if (dir == false)
        //    {  
        //        Directory.CreateDirectory(@"D:\webapp\WebAPP\02Controller\Photos\rat");
        //    }
        //    else
        //    {
        //        MessageBox.Show("The folder has existed");
        //    }


        //    if (Directory.Exists(@"D:\webapp\WebAPP\02Controller\Photos\rat"))
        //    {
        //        System.IO.File.Create(@"D:\webapp\WebAPP\02Controller\Photos\rat\test.txt");


        //        if (!System.IO.File.Exists(@"D:\webapp\WebAPP\02Controller\Photos\rat\test.txt"))
        //        {
        //            System.IO.File.Create(@"D:\webapp\WebAPP\02Controller\Photos\rat\test.txt").Close();

        //        }



               

           // }
       // }


    }
}