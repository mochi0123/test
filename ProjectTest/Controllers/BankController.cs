﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualBasic;
using ProjectTest.Models;

namespace ProjectTest.Controllers
{
    public class BankController : Controller
    {
        Web5Entities db = new Web5Entities();
        // GET: Bank
        public ActionResult Index()
        {
            //BankAccount bankAccount = new BankAccount
            //{
            //    BankAccount_Account = account
            //};

           var account = db.BankAccount.FirstOrDefault().BankAccount_Account.ToString();   

            if (db.BankAccount != null)
            {
                
                MaskCreditcard(account);
                //account=db.BankAccount.FirstOrDefault().BankAccount_Account.Substring(0, 4) + "******" + account.Substring(13, 4);

            }
        var bank = db.BankAccount.ToList();
            return View(bank);
        }

        public struct RegularExp
        {
            public const string creditcard = @"^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$";
        }

        /// 信用卡遮罩--信用卡號=>前6後4不mask
        /// 
        ///5555-2525-1266-2213
        /// 
        /// 5555-25**-****-2213
        /// 5555 2525 1266 2213
        /// 
        private static string MaskCreditcard(string account)
        {
            if (!string.IsNullOrEmpty(Strings.StrConv(account, VbStrConv.Narrow, 0)))
            { 
               // account = account.Substring(0, 4) + "******" + account.Substring(13, 4);
                //account = (Regex.IsMatch(account,RegularExp.creditcard) ? account.Substring(0, 4) + "**-****" + account.Substring(13, 4) : "");
                account = account.Substring(0, 4) + "********" + account.Substring(12, 4) ;
            }
            else
            {
                return "";
            }
            return account;
        }

        public PartialViewResult _BankAccount(string id)
        {
            var b = db.BankAccount.Where(m => m.BankAccount_OwnerID == id);
            
            if(b.Count() ==0)
            {
                ViewBag.create = "1";

            }

            else
                ViewBag.create = "0";

            var bankAccount = db.BankAccount.Where(m => m.BankAccount_OwnerID == id).ToList();

            
            return PartialView(bankAccount);
        }

        public ActionResult CreateBank()
        {
            BankAccount newbank = new BankAccount();

            var issuer = db.Issuer.ToList();

            return View(issuer);
        }

        [HttpPost]
        public ActionResult CreateBank(string BankAccount_Account, string Issuer_Name)
        {
            var id = Session["User_ID"].ToString();

            BankAccount bankAccount = new BankAccount
            {
                BankAccount_Account = BankAccount_Account,
                BankAccount_IssuerNo = Issuer_Name,
                BankAccount_OwnerID = id
            };
            

            db.BankAccount.Add(bankAccount);
            db.SaveChanges();


            return RedirectToAction("ShowCreditCard","CreditCard", new { id = id });
        }

        public ActionResult Delete(string id)
        {
            var bankaccount = db.BankAccount.Where(m => m.BankAccount_OwnerID == id).FirstOrDefault();
            db.BankAccount.Remove(bankaccount);
            db.SaveChanges();

            return RedirectToAction("ShowCreditCard","CreditCard", new { id = id });
        }

    }
}