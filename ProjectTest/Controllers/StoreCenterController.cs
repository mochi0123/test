﻿using ProjectTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectTest.Controllers
{
    public class StoreCenterController : Controller
    {
        Web5Entities db = new Web5Entities();

        // GET: StoreCenter
        //賣家中心首頁:需要登入，訪客身分會導向登入頁面
        public ActionResult Index()
        {
            if (Session["User_ID"] != null) return View(); 

            return RedirectToAction("HomePage","WebHome");
        }
    }
}