﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Windows;
using ProjectTest.Models;

namespace ProjectTest.Controllers
{
    public class ViewProductController : Controller
    {
        Web5Entities db = new Web5Entities();
        // GET: ViewProduct
        public ActionResult Index()
        {
            return View();
        }

        //顯示商品詳細資料
        public ActionResult Home(string no)
        {
            //若使用網址輸入錯誤的變數，將返回首頁
            if (db.Product.Find(no) == null) return RedirectToAction("HomePage", "WebHome");

            //linq取商品資料
            var p_result = db.Product.Find(no);
            ViewBag.min_price = db.view_ProductDetails.Where(m => m.P_No == no).FirstOrDefault().Min_Price;
            ViewBag.max_price = db.view_ProductDetails.Where(m => m.P_No == no).FirstOrDefault().Max_Price;
            ViewBag.Fnum = p_result.P_ProductFormatNo2 == "PF00000000" ? 1 : 2;

            //linq取商品規格一的選項列表
            var option1_result = (from a in db.ProductOption
                                  where a.POption_ProductNo == no
                                  select a.POption_Name1).Distinct();
            //linq取商品規格二的選項列表
            var option2_result = (from a in db.ProductOption
                                  where a.POption_ProductNo == no
                                  select a.POption_Name2).Distinct();

            //linq取商品運送資料
            var fare = from ps in db.view_ProductShip
                       where ps.P_No == no
                       select ps;
            //linq取商品付款資料
            var pay = from pt in db.view_ProductPay
                      where pt.P_No == no
                      select pt;

            //Product:商品資料,Option1:商品規格一的選項,Option2:商品規格二的選項
            ViewProduct vp = new ViewProduct()
            {
                product = p_result,
                Option1 = option1_result.ToList(),
                Option2 = option2_result.ToList(),
                ShipFare = fare.ToList(),
                Pay = pay.ToList()
            };
            //抓取商品編號進Session供留言板使用
            Session["P_No"] = no;





            return View(vp);

        }
        //抓取商品選項的資料
        public JsonResult FindOption(string p_no) {

            var result = db.ProductOption.Where(m => m.POption_ProductNo == p_no);

            JsonResult json = Json(new {
                
                    options = result.Select(item => new
                    {
                        name1 = item.POption_Name1,
                        name2 = item.POption_Name2,
                        pic = item.POption_Pic,
                        price = item.POption_Price,
                        p_no = item.POption_ProductNo,
                        stock = item.POption_stock
                    })
                
            });

            //JsonResult json = Json(new {
            //    pic = result.FirstOrDefault().POption_Pic,
            //    price = result.FirstOrDefault().POption_Price
            //});
            //if (result.Count() > 1)
            //{
            //    json = Json(new
            //    {
            //        pic = result.Where(m => m.POption_Name2 == option2).FirstOrDefault().POption_Pic,
            //        price = result.Where(m => m.POption_Name2 == option2).FirstOrDefault().POption_Price
            //    });
            //}

            return json;
        }

        
        //抓取要加入購物車的資料:商品編號、選項一、選項二、數量，並傳回購物車內資料筆數
        public string Cart(string p_no, string option1, string option2, string number)
        {
            if (Session["User_ID"]==null) return "";

            var id = Session["User_ID"].ToString();
            //var repro = db.Cart.Where(m => m.Cart_OwnerID == id && m.Cart_ProductOptionNo == p_no && m.ProductOption.POption_Name1 == option1 && m.ProductOption.POption_Name2 == option2).FirstOrDefault();

            //if (repro == default(Cart) )
            //{
            //    Cart cart1 = new Cart();

            //    cart1.Cart_ProductNumber=
            //    Cart_ProductNumber = Int32.Parse(number),
            //                }

            Cart cart = new Cart()
            {
                Cart_OwnerID = id,
                Cart_ProductOptionNo = FindOptionNo(p_no, option1, option2),
                Cart_ShowStatus = true,
                Cart_ProductNumber = Int32.Parse(number),        
            };


            var checkpno = db.Cart.Where(m => m.Cart_OwnerID == id);
            var po_no = FindOptionNo(p_no, option1, option2);

            checkpno = checkpno.Where(m=>m.Cart_ProductOptionNo == po_no);

            var checkpolist = checkpno.ToList();

            if (checkpolist.Count()==0)
            {
                db.Cart.Add(cart);
                db.SaveChanges();
            }
            else {

                var pono = checkpolist.FirstOrDefault().Cart_ProductNumber;
                pono += Int32.Parse(number);

                checkpno.FirstOrDefault().Cart_ProductNumber = pono;


                db.SaveChanges();
            }
           
            return "";
        }

        public string FindOptionNo(string p_no, string option1, string option2)
        {
            string po_no = "";

            if (option1 == "0" && option2 == "0")
            {
                po_no = db.ProductOption.Where(m => m.POption_ProductNo == p_no).First().POption_No;
            }
            else if (option2 == "0")
            {
                po_no = db.ProductOption.Where(m => m.POption_ProductNo == p_no && m.POption_Name1 == option1).First().POption_No;
            }
            else
            {
                po_no = db.ProductOption.Where(m => m.POption_ProductNo == p_no && m.POption_Name1 == option1 && m.POption_Name2 == option2).First().POption_No;
            }

            return po_no;
        }


        //-------留言趴修VIEW
        [ChildActionOnly]
        public PartialViewResult _Comment()
        {
            var P_No = Session["P_No"].ToString();
            List<Comment> comment;
            comment = db.Comment.Where(m=>m.Comment_ProductNo== P_No).OrderByDescending(m => m.Comment_Date).ThenByDescending(m => m.Comment_No).ToList();
            return PartialView("_Comment", comment);
        }
        
        //------新增留言趴修VIEW
        [ChildActionOnly]
        public PartialViewResult _ComCreat()
        {
            //ViewBag.from = "x";
            //if (Session["User_ID"] == null) ViewBag.from = "_ComCreat";

            return PartialView("_ComCreat");
        }
        [HttpPost]
        public PartialViewResult _ComCreat(Comment com)
        {
            if (Session["User_ID"] == null)
            {
                TempData["Message"] = "請登入再留言";

                Response.Redirect(Request.Url.ToString());//網頁重整


                return PartialView("_ComCreat");
            }

            com.Comment_Date = DateTime.Now;//取得留言時間
            com.Comment_OwnerID = Session["User_ID"].ToString();//取得留言者ID           
            com.Comment_ReportStatus = false;
            com.Comment_ProductNo = Session["P_No"].ToString();

            db.Comment.Add(com);
            db.SaveChanges();
            Response.Redirect(Request.Url.ToString());//網頁重整
            return PartialView("_ComCreat");
        }


    }
}