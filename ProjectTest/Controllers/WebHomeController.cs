﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectTest.Models;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity.Validation;
using PagedList;
using System.Net.Mail;
using System.Configuration;

namespace ProjectTest.Controllers
{
    public class WebHomeController : Controller
    {
        Web5Entities db = new Web5Entities();
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyWeb5"].ConnectionString);
        SqlCommand cmd;

        // GET: WebHome
        public ActionResult Index()
        {
            return View(db.User.ToList());
        }

        //首頁
        public ActionResult HomePage(string pclass="1",int page=1)
        {
            var result = db.view_HomepageProduct.Where(m => m.P_ProductClassNo == pclass).OrderBy(m=>m.P_No);
            var r = from t1 in result
                    join t2 in db.Product on t1.P_No equals t2.P_No
                    where t2.ProductStatus.PS_No == "1"
                    select t1;

            ViewBag.pclass = pclass;

            HomePage hp = new HomePage() {
                productclass = db.ProductClass.ToList(),
                view_product = r.OrderBy(m => m.P_No).ToPagedList(page,6)
            };

            //廣告版面配置
            //輪播廣告1~3
            string[] AD = new string[7];
            string[] Url = new string[7];
            for (var i = 1;i<= AD.Length; i++) {
                var temp = db.Ad.Where(m => m.Ad_Layoutlocation == i.ToString());
                if (temp.Count() > 0)
                { 
                    AD[i - 1] = temp.First().Ad_Pic;
                    if (temp.First().Ad_URL != null) Url[i - 1] = temp.First().Ad_URL;
                    else Url[i - 1] = "#";
                }
                else { 
                    AD[i - 1] = "p0.jpg";
                }

            }
            ViewBag.AD = AD;
            ViewBag.Url = Url;

            return View(hp);
        }

        //註冊
        public ActionResult Create()
        {
            //預設性別為男
            User u = new User();            
            u.User_Gender = true;
            //預設生日為當天
            u.User_Birth = DateTime.Now;

            return View(u);
        }
        [HttpPost]
        public ActionResult Create(User u,string Password)
        {
            //確認密碼
            if (u.User_Password != Password)
            {
                ViewBag.checkpwd = "與密碼不相符!";
                //資料輸入錯誤，回到註冊頁面
                return View(u);
            }

            //檢查生日
            if (u.User_Birth >= DateTime.Now)
            {
                ViewBag.checkbirth = "生日輸入錯誤!";
                //資料輸入錯誤，回到註冊頁面
                return View(u);
            }

            //檢查會員編碼用完
            if (getUserID() <= 999999999)
                u.User_ID = "U" + getUserID().ToString("D9");
            else {
                ViewBag.error += "會員已達上限!<br>";
                //資料輸入錯誤，回到註冊頁面
                return View(u);
            }

            //未填寫的資料行預設
            u.User_StoreName = u.User_BuyerName;
            u.User_Photo = "";
            u.User_Address = "";
            u.User_StoreCoverPic = "";
            u.User_StoreIntroduction = "";
            u.User_StoreProductDisplay = "";
            u.User_FbID = "";
            u.User_Status = "x";
            u.User_StoreStatusNo = "2";

            //是否通過資料驗證
            if (ModelState.IsValid)
            {
                db.User.Add(u);

                try
                {
                    db.SaveChanges();

                    SendAuthEmail(u.User_ID,"重發驗證信");
                    ViewBag.Message = "加入會員完成,請收取驗證信以完成所有加入會員程序!!";
                }
                catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
                {
                    //sql server 報錯，通常是UQ的重複問題
                    ViewBag.error += "輸入資料已註冊，請修改!!";
                    //資料輸入錯誤，回到註冊頁面
                    return View(u);
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    //sql server 報錯，通常是datatype跟nou null問題
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
                //資料新增成功，回到首頁
                //return RedirectToAction("HomePage", "WebHome");
                //資料新增成功頁面跳轉
                return View("AuthEmail");
            }
            else
            {
                //model的資料驗證報錯
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        Console.WriteLine(error);
                    }
                }
                return View(u);
            }
        }

        //登入
        public JsonResult Login(string Acount, string Password)
        {
            var result = db.User.Where(m => m.User_Account == Acount && m.User_Password == Password).FirstOrDefault();
            var msg = "";

            //檢查輸入的帳號密碼是否正確
            if (result != null)
            {
                //驗證帳號是否凍結
                if (result.User_Status == "1") msg = "帳號已被凍結請洽客服協助處理!!";
                //驗證帳號是否未開通
                else if (result.User_Status == "x") msg = "帳號未開通，請收取驗證信開通帳號!!";
                else
                {
                    Session["User_ID"] = result.User_ID;
                    Session["User_BuyerName"] = result.User_BuyerName;
                    Session["User_Photo"] = result.User_Photo;
                    msg = "登入成功!!";
                }
            }
            else
            {
                //登入失敗
                msg = "帳號或密碼錯誤!!";
            }

            return Json(new { msg = msg });
        }


        //舊的登入
        //public ActionResult Login()
        //{
        //    return View();
        //}
        //[HttpPost]
        //public ActionResult Login(string Acount, string Password)
        //{
        //    var result = db.User.Where(m=>m.User_Account == Acount && m.User_Password == Password).FirstOrDefault();


        //    //檢查輸入的帳號密碼是否正確
        //    if (result != null)
        //    {
        //        //驗證帳號是否凍結
        //        if (result.User_Status == "1")
        //        {
        //            ViewBag.Msg = "帳號已被凍結請洽客服協助處理!!";
        //            return View();
        //        }
        //        //驗證帳號是否未開通
        //        else if (result.User_Status == "x")
        //        {
        //            ViewBag.Msg = "帳號未開通，請收取驗證信開通帳號!!";
        //            ViewBag.resend = "沒收到驗證信?";
        //            return View();
        //        }
        //        else {
        //            ViewBag.Message = "登入成功!!";
        //            Session["User_ID"] = result.User_ID;
        //            Session["User_BuyerName"] = result.User_BuyerName;
        //            //Session["Acount"] = Acount;
        //            //成功登入，回到首頁
        //            return RedirectToAction("HomePage", "WebHome");
        //        }
        //    }
        //    else
        //    {
        //        ViewBag.Msg = "帳號或密碼錯誤!!";
        //    }
        //    //登入失敗，回到登入頁面
        //    return View();
        //}

        //登出
        public ActionResult Logout()
        {
            Session["User_ID"] = null;
            //Session["Acount"] = null;
            //登出成功，回到首頁
            return RedirectToAction("HomePage", "WebHome");
        }

        //取得註冊者可用的id數字
        public int getUserID()
        {
            string id = "";
            var result = db.User.OrderByDescending(m=>m.User_ID).Take(1);
            
            foreach(var r in result)
                id = r.User_ID;
            id = id.ToString().Substring(1, 9);
            int i = Int32.Parse(id) + 1;
            //id = "U" + i.ToString("D9");

            return i;
        }

        //寄出驗證信
        private void SendAuthEmail(string id,string type)
        {
            SmtpClient client = new SmtpClient("msa.hinet.net");
            string email = db.User.Find(id).User_Email;

            string from = "UGo5@gmail.com";
            string to = email;
            string subject = "";
            string body = "";

            switch (type) {
                case "忘記密碼":
                    subject = "【U購站】會員重設密碼信";
                    body = "請點擊以下超連結以重設密碼\n\nhttp://localhost:49636/WebHome/ResetPwd?id=" + id;
                    break;
                case "重發驗證信":
                    subject = "【U購站】加入會員驗證信";
                    body = "請點擊以下超連結以完成驗證\n\nhttp://localhost:49636/WebHome/AuthEmail?id=" + id;
                    break;
                default:
                    break;
            }

            client.Send(from, to, subject, body);

        }

        public ActionResult ResetPwd(string id) {
            ViewBag.id = id;
            return View();
        }

        [HttpPost]
        public ActionResult ResetPwd(string id,string pwd)
        {
            var member = db.User.Find(id);
            if (member == null)
            {
                ViewBag.Message = "您可能是從不當的方式進入";
                return RedirectToAction("Login");
            }

            string sql = "update [User] set User_Password=@pwd where User_ID=@id";
            cmd = new SqlCommand(sql, Conn);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@pwd", pwd);

            Conn.Open();
            cmd.ExecuteNonQuery();
            Conn.Close();

            ViewBag.Message = "密碼已重設成功，請重新登入!!";
            return View("AuthEmail");
        }

        //透過驗證連結開通帳號
        public ActionResult AuthEmail(string id)
        {
            var member = db.User.Find(id);
            if (member == null)
            {
                ViewBag.Message = "您可能是從不當的方式進入,請按驗證程序完成流程";
                return View();
            }
            if (member.User_Status == "x")
            {
                string sql = "update [User] set User_Status=@status where User_ID=@id";
                cmd = new SqlCommand(sql, Conn);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@status", "0");

                Conn.Open();
                cmd.ExecuteNonQuery();
                Conn.Close();

                ViewBag.Message = "驗證完成，請重新登入!!";
            }
            else ViewBag.Message = "此連結已失效";
            return View();

        }

        //重寄信
        public ActionResult ReSendEmail(string type) {
            ViewBag.type = type;
            return View();
        }

        [HttpPost]
        //重寄信
        public ActionResult ReSendEmail(string email, string type) {

            var member = db.User.Where(m=>m.User_Email == email);
            if (member.Count() < 1)
            {
                ViewBag.NotFind = "您輸入的信箱查無結果，請重新輸入!!";
                ViewBag.type = type;
                return View("ReSendEmail");
            }
            else
            {
                SendAuthEmail(member.First().User_ID,type);
                switch (type)
                {
                    case "忘記密碼":
                        ViewBag.Message = "重設密碼信已寄出至填寫信箱!!";
                        break;
                    case "重發驗證信":
                        ViewBag.Message = "驗證信已重新發送!!";
                        break;
                    default:
                        ViewBag.Message = "x";
                        break;
                }
                    
                return View("AuthEmail");
            }
            
        }
        public ActionResult Help()
        {
            return View();
        }

    }
}