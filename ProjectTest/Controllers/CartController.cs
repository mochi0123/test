﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectTest.Models;

namespace ProjectTest.Controllers
{
   
    public class CartController : Controller
    {
       
        Web5Entities db = new Web5Entities();
        // GET: Cart
        public ActionResult Index()
        {
            var cart = db.Cart.ToList();
            

            return View(cart);
        }

        public ActionResult ShowCar(string id)
        {
            
            var car = db.Cart.Where(m => m.Cart_OwnerID == id).ToList();


            //var p = db.Cart.Where(m => m.Cart_OwnerID == id).Where(m => m.Cart_ProductOptionNo == m.ProductOption.POption_No);           
            return View(car);

        }


        public ActionResult ChangeNum(string id,string p_no,int num1)
        {

            var car = db.Cart.Where(m => m.Cart_OwnerID == id && m.Cart_ProductOptionNo == p_no).FirstOrDefault();


            //var p = db.Cart.Where(m => m.Cart_OwnerID == id).Where(m => m.Cart_ProductOptionNo == m.ProductOption.POption_No);           
            return View(car);

        }


        public void SaveCartNumber(int num, string id, string id2)
        {
            // string id = Session["User_ID"].ToString();
            //Cart cart = new Cart();

            var result = db.Cart.Where(m => m.Cart_OwnerID == id && m.Cart_ProductOptionNo == id2).FirstOrDefault();

            result.Cart_ProductNumber = num;
            db.SaveChanges();

            //JsonResult json = Json(new
            //{
            //    num = result.FirstOrDefault().Cart_ProductNumber

            //});


        }

        public ActionResult Delete(string id)
        {
            
            var car = db.Cart.Where(m => m.Cart_OwnerID == id).ToList();
            db.Cart.RemoveRange(car);
            db.SaveChanges();

            return RedirectToAction("ShowCar", new { id = id });
        }

        public ActionResult DelPro(string id, string po_no)
        {

            var dcar = db.Cart.Where(m => m.Cart_OwnerID == id && m.Cart_ProductOptionNo == po_no).FirstOrDefault();
            db.Cart.Remove(dcar);
            db.SaveChanges();

            return RedirectToAction("ShowCar", new { id = id });
        }



        //public JsonResult ClearCart(string id, string num)
        //{

        //    var result = db.Cart.Where(m => m.Cart_OwnerID == id);

        //    JsonResult json = Json(new
        //    {
        //        num = result.FirstOrDefault().Cart_ProductNumber

        //    });

        //    return json;
        //}

        //public JsonResult DeleteCart(string id, string po_no)
        //{
        //    var dcar = db.Cart.Where(m => m.Cart_OwnerID == id && m.Cart_ProductOptionNo == po_no).FirstOrDefault();

        //    db.Cart.Remove(dcar);
        //    db.SaveChanges();

        //    return;
        //}

        //public ActionResult Delete(string id)
        //{

        //    var car = db.Cart.Where(m => m.Cart_OwnerID == id).ToList();
        //    db.Cart.RemoveRange(car);
        //    db.SaveChanges();

        //    return RedirectToAction("ShowCar", new { id = id });
        //}



    }
    }
