﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Windows.Forms;

namespace ProjectTest.Controllers
{
    public class UserPicController : Controller
    {
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyWeb5"].ConnectionString);

        SqlCommand Cmd = new SqlCommand();

        SqlDataAdapter adp = new SqlDataAdapter();

        private DataTable querySql(string sql)
        {
            Cmd.CommandText = sql;
            Cmd.Connection = Conn;
            adp.SelectCommand = Cmd;

            DataSet ds = new DataSet();
            adp.Fill(ds);

            return ds.Tables[0];
        }

        public ActionResult Edit(string id)
        {

            if (Directory.Exists(@"D:\Users\C50105\source\repos\test\ProjectTest\photo\user_pic\"+id))
            {
                //資料夾存在
            }
            else
            {
                //新增資料夾
                Directory.CreateDirectory(@"D:\Users\C50105\source\repos\test\ProjectTest\photo\user_pic\" + id);
            }


            string sql = "select User_Photo from [User]  where User_ID=@User_ID";
            Cmd.Parameters.AddWithValue("@User_ID", id);
            DataTable dt = querySql(sql);
            return View(dt);
        }


        [HttpPost]
        public ActionResult EditUserPic(string Hidden1)
        {
            string sql = "update [User] set User_Photo=@User_Photo  where User_ID=@User_ID";  

            SqlCommand Cmd = new SqlCommand(sql, Conn);
         
            Cmd.Parameters.AddWithValue("@User_Photo", Hidden1);
            Cmd.Parameters.AddWithValue("@User_ID", Session["User_ID"]);

            Conn.Open();
            Cmd.ExecuteNonQuery();
            Conn.Close();

            //return Redirect(Request.UrlReferrer.ToString());
            Session["User_Photo"] = Hidden1;
            return RedirectToAction("Edit", "UserPic",new{id=Session["User_ID"]});

        }

        //更改並儲存上傳的圖檔名
        public string EditPic()
        {
            var pic = Request.Files["pic"]; 
            var file_name = "";
            if (pic != null)
            {
                if (pic.ContentLength > 0)
                {
                    file_name = DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "").Replace("上午", "").Replace("下午", "") + ".jpg";

                    pic.SaveAs(Server.MapPath("~/photo/user_pic/" + file_name));
                }
            }

            return file_name;
        }


        //02-4-5 建立GET與POST的Create方法
        // GET: FileUpload
        //public ActionResult Create()
        //{
        //    return View();
        //}


        //[HttpPost]
        //public ActionResult Create(HttpPostedFileBase photo)
        //{

        //    string fileName = "";

        //    if (photo != null)
        //    {

        //        if (photo.ContentLength > 0)
        //        {
        //            fileName = photo.FileName;
        //            fileName = System.IO.Path.GetFileName(fileName);


        //            photo.SaveAs(Server.MapPath("~/photo/user_pic/" + fileName));
        //        }
        //    }
        //    //return View();
        //    return RedirectToAction("ShowPhotos");
        //}

        public string ShowPhotos()
        {
            string show = "";

            //建立一個可以操作資料夾的物件
            DirectoryInfo dir = new DirectoryInfo(Server.MapPath("~/Photo/user_pic"));

            //把取得的所有檔案放到FileInfo物件陣列裡
            FileInfo[] fInfo = dir.GetFiles();

            //用迴圈逐一將檔名讀出,並放入img元素的src屬性
            foreach (FileInfo result in fInfo)
            {
                show += "<a href='../Photo/user_pic" + result.Name + "'><img src='../Photos/" + result.Name + "' width='100'></a>";
            }
            show += "<p><a href='Create'>返回Create</a></p>";
            return show;
        }
    }
}