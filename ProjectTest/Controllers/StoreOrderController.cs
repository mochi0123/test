﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectTest.Models;

namespace ProjectTest.Controllers
{
    public class StoreOrderController : Controller
    {
        Web5Entities db = new Web5Entities();
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyWeb5"].ConnectionString);
        SqlCommand cmd = new SqlCommand();


        [LoginRule]
        public ActionResult Index()
        {
            //if (Session["User_ID"] == null) return RedirectToAction("Login", "WebHome");
            //var i = from a in db.Product join b in db.ProductOption on a.P_No equals b.POption_ProductNo 
            //            select new { product = a.P_No, productoption = b.POption_Name1 };

            //ViewBag.a = i.ToList();

            return View();
        }

        [ChildActionOnly]
        public ActionResult _PartialOrder(string status_no="0") {
            string id = Session["User_ID"].ToString();
            var temp = db.ProductOrder.Include("ProductOrderDetails").Select(pod => new { pod.PO_No, Seller = pod.ProductOrderDetails.Select(po=>po.ProductOption).Select(p=>p.Product.P_OwnerID).Distinct().FirstOrDefault()});
            var order = db.ProductOrder;
            var result0 = from t in temp join o in order on t.PO_No equals o.PO_No where t.Seller == id select o;
            var result1 = result0.Where(m => m.PO_ShipStatusNo == "1");
            var result3 = result0.Where(m => m.PO_ShipStatusNo == "3");
            var result4 = result0.Where(m => m.PO_ShipStatusNo == "4");

            int[] count = new int[5] { result0.Count(), result1.Count(),0, result3.Count(), result4.Count()};
            ViewBag.count = count;
            ViewBag.status = Int32.Parse(status_no);

            var result = result0;
            switch (status_no)
            {
                case "1":
                    result = result1;
                    break;
                case "3":
                    result = result3;
                    break;
                case "4":
                    result = result4;
                    break;
                default:
                    break;
            }

            return PartialView("_PartialOrder", result.ToList());
        }


        public ActionResult Edit(string no ,string ship_type) {
            string ship_status = "";
            if (ship_type == "宅配") ship_status = "3";
            else ship_status = "4";

            string sql = "update [ProductOrder] set PO_ShipStatusNo=@ship_status where PO_No=@no";
            cmd.CommandText = sql;
            cmd.Connection = Conn;
            cmd.Parameters.AddWithValue("@ship_status", ship_status);
            cmd.Parameters.AddWithValue("@no", no);
            Conn.Open();
            cmd.ExecuteNonQuery();
            Conn.Close();

            return RedirectToAction("Index");
        }

        [ChildActionOnly]
        public ActionResult _PartialDetails(string po_no) {
            var result = db.ProductOrderDetails.Where(m => m.PODetails_ProductOrderNo == po_no);

            return PartialView("_PartialDetails",result.ToList());
        }
    }
}