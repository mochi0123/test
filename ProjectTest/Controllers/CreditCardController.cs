﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectTest.Models;


namespace ProjectTest.Controllers
{
    public class CreditCardController : Controller
    {
        Web5Entities db = new Web5Entities();
        // GET: CreditCard
        public ActionResult Index()
        {
            var credit = db.CreditCard.ToList();

            return View(credit);
        }

      
        public ActionResult ShowCreditCard(string id)
        {
            var r = db.CreditCard.Where(m => m.CreditCard_OwnerID == id);
            if (r.Count() == 0)
            {
                ViewBag.create = "1";
               
            }
            else
                ViewBag.create = "0";

            var creditcard = db.CreditCard.Where(m => m.CreditCard_OwnerID == id).ToList();
            //var creditcard =( from m in db.CreditCard
            //                 join p in db.Issuer on m.CreditCard_IssuerNo equals p.Issuer_No
            //                 where m.CreditCard_OwnerID == id
            //                  select new { m.CreditCard_No, m.CreditCard_OwnerID, p.Issuer_Name, p.CreditCard }).ToList();

           

            ViewBag.User_ID = id;
           

            return View(creditcard);
        }

       
        public ActionResult Create()
        {


            CreditCard newCredit = new CreditCard();

            var issuer = db.Issuer.ToList();



            return View(issuer);
        }

      
        [HttpPost]
        public ActionResult Create(string CreditCard_No,string Issuer_Name)
        {
            var id = Session["User_ID"].ToString();
            CreditCard creditCard = new CreditCard{
                CreditCard_No = CreditCard_No,
                CreditCard_IssuerNo=Issuer_Name,
                CreditCard_OwnerID = id
            };

            db.CreditCard.Add(creditCard);
            db.SaveChanges();
            

            return RedirectToAction("ShowCreditCard",new { id=id});
        }


        public ActionResult Delete(string id)
        {
            var credit = db.CreditCard.Where(m => m.CreditCard_OwnerID == id).FirstOrDefault();
            db.CreditCard.Remove(credit);
            db.SaveChanges();

            return RedirectToAction("ShowCreditCard", new { id = id });
        }




    }
}