﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectTest.Models;

namespace ProjectTest.Controllers
{
    public class PayController : Controller
    {
        Web5Entities db = new Web5Entities();
        // GET: Pay
        public ActionResult Index()
        {
            var payment = db.ProductOrder.ToList();
            return View();
        }


        public ActionResult ShowOrders(string id)
        {
            var r = db.ProductOrder.Where(m => m.PO_OwnerID == id);
            if (r.Count() == 0)
            {
                ViewBag.Po = "0";

            }
            else
            {
                ViewBag.Po = "1";

            }
            var productOrders = db.ProductOrder.Where(m => m.PO_OwnerID == id).OrderByDescending(m=>m.PO_CreateDate).ToList();
            

           

            return View(productOrders);

        }

        public ActionResult ShowOrderDetail(string po_no)
        {
           

            var productOrderdetail = db.ProductOrderDetails.Where(m => m.PODetails_ProductOrderNo == po_no).ToList();




            return View(productOrderdetail);

        }

        public ActionResult Create(string id)
        {
            //var order = db.Cart.Where(m => m.Cart_OwnerID == id).ToList();
            //ProductOrder newProductOrder = new ProductOrder();

            var order = db.Cart.Include("ProductOption").Where(m => m.Cart_OwnerID == id).OrderBy(m => m.ProductOption.Product.P_OwnerID).Select(m => new
            {

                p_optionNo = m.Cart_ProductOptionNo,
                ppic = m.ProductOption.POption_Pic,
                pname = m.ProductOption.Product.P_Name,
                pseller = m.ProductOption.Product.User.User_StoreName,
                pnumber = m.Cart_ProductNumber,
                poption1 = m.ProductOption.POption_Name1,
                poption2 = m.ProductOption.POption_Name2,
                pprice = m.ProductOption.POption_Price,
                username = m.User.User_TrueName,
                useremail = m.User.User_Email,
                useraddress = m.User.User_Address,
                userphone = m.User.User_Phone,
                // pstock = m.ProductOption.POption_stock,
                
            }); 

            ViewBag.Order = order.ToList();

            ViewBag.OrderCount = order.Count();

           

            ViewCart viewCart = new ViewCart
            {
                payTypes = db.PayType.ToList(),
                shipTypes = db.ShipType.ToList(),
                creditCards = db.CreditCard.Where(m => m.CreditCard_OwnerID == id).ToList(),
                bankAccounts = db.BankAccount.Where(m => m.BankAccount_OwnerID == id).ToList()

            };


            //var result = from c in db.Product
            //             join s in db.ProductOption on c.P_No equals s.POption_ProductNo
            //             join a in db.Cart on s.POption_No equals a.Cart_ProductOptionNo into p
            //             where a.Cart_OwnerID == id
            //             orderby c.P_OwnerID
            //             select new { po_pic = s.POption_Pic, p_name = c.P_Name, p_price = s.POption_Price, p_number = a.Cart_ProductNumber, po_name1 = s.POption_Name1, po_name2 = s.POption_Name2, shopname = c.User.User_StoreName, pod_no = s.POption_No };


            //ViewBag.Seller = result.ToList();

            //var sellers= (from p in db.Product
            //              group p by p.P_OwnerID).ToList();


            //var result_group= from c in db.Product
            //                  join s in db.ProductOption on c.P_No equals s.POption_ProductNo
            //                  join a in db.Cart on s.POption_No equals a.Cart_ProductOptionNo
            //                  //where a.Cart_OwnerID == id
            //                  into groupseller
            //                  orderby c.P_OwnerID
            //                  select new { po_pic = s.POption_Pic, p_name = c.P_Name, p_price = s.POption_Price, p_number = a.Cart_ProductNumber, po_name1 = s.POption_Name1, po_name2 = s.POption_Name2, shopname = c.User.User_StoreName, pod_no = s.POption_No };


            return View(viewCart);
        }

        

        //[HttpPost]
        //public ActionResult Create(string po_no, DateTime po_createdate,string po_paytype,string po_shipytpeno,DateTime po_canceladate,int po_total,string po_ownerid,)
        //{
        //    var id = Session["User_ID"].ToString();
        //    ProductOrder productOrder = new ProductOrder
        //    {
        //        PO_No = po_no,
        //        PO_CreateDate=po_createdate,
        //        PO_PayTypeNo=po_paytype,
        //        PO_ShipTypeNo=po_shipytpeno,
        //        PO_CancelDate=po_canceladate,
        //        PO_Total=po_total,


        //        CreditCard_No = CreditCard_No,
        //        CreditCard_IssuerNo = Issuer_Name,
        //        CreditCard_OwnerID = id
        //    };

        //    db.ProductOrder.Add(productOrder);
        //    db.SaveChanges();


        //    return RedirectToAction("ShowCreditCard", new { id = id });
        //}

        [HttpPost]
        public ActionResult Create(string[] PO_ShipTypeNo, string[] PO_Receiver,string[] PO_PayTypeNo,int[] PO_Total,string[] PO_RecipientAddress,string[] PO_RecipientEmail,string[] PO_RecipientPhone,string[] PODetails_ProductOptionNo,int[] PODetails_Number)
        {
            var id = Session["User_ID"].ToString();
            var seller = db.Cart.Include("ProductOption").Where(m => m.Cart_OwnerID == id).OrderBy(m => m.ProductOption.Product.P_OwnerID).Select(m => new { s = m.ProductOption.Product.User.User_StoreName }).ToList();
            string[] store = new string[seller.Count()];
            var k = 0;
            foreach (var item in seller)
            {
                store[k] = item.s;
                k++;
            }

            string[] no = new string[PO_ShipTypeNo.Length];
            ProductOrder order = new ProductOrder();
            for (var i = 0; i < PO_ShipTypeNo.Length; i++)
            {
                no[i] = GetRandomStringByFileName();
                order = new ProductOrder
                {
                    PO_No = no[i],
                    PO_ShipTypeNo = PO_ShipTypeNo[i],
                    PO_Receiver = PO_Receiver[i],
                    PO_CreateDate=DateTime.Now,
                    PO_OwnerID=id,
                    PO_PayTypeNo= PO_PayTypeNo[0],
                    PO_Total= PO_Total[i],
                    PO_ProductOrderStatusNo="1",
                    PO_ShipStatusNo="1",
                    PO_RecipientAddress= PO_RecipientAddress[i],
                    PO_RecipientEmail= PO_RecipientEmail[i],
                    PO_RecipientPhone= PO_RecipientPhone[i],
                };
                db.ProductOrder.Add(order);
            }

            var now = "";
            var next = "";
            var j = 0;
            ProductOrderDetails details = new ProductOrderDetails();
            for (var i = 0; i < seller.Count(); i++)
            {
                details = new ProductOrderDetails
                {
                    PODetails_ProductOrderNo = no[j],
                    PODetails_ProductOptionNo= PODetails_ProductOptionNo[i],
                    PODetails_Number= PODetails_Number[i],

                };
                if (i < seller.Count() - 1)
                {
                    now = store[i];
                    next = store[i + 1];
                    if (now != next) j++;
                }
                db.ProductOrderDetails.Add(details);
            }

            Response.Write(order);
            Response.Write(details);
            db.SaveChanges();

            //ViewBag.number = db.Cart.Where(m => m.Cart_OwnerID == id).FirstOrDefault().Cart_ProductNumber;
            //ViewBag.pronum = db.Cart.Where(m => m.Cart_OwnerID == id).ToList().Count();
            //ViewBag.sellernum = db.Cart.Where(m => m.Cart_OwnerID == id).GroupBy(m=>m.ProductOption.Product.P_OwnerID).Count();
            ////var id = Session["User_ID"].ToString();

            //for (int i=0;i < ViewBag.sellernum; i++)
            //{
            //    ViewBag.pono = GetRandomStringByFileName();

            //    ProductOrder productOrder = new ProductOrder
            //{
            //    PO_No= ViewBag.pono,
            //    PO_CreateDate = DateTime.Now,
            //    //PO_PayTypeNo = po.PO_PayTypeNo,

            //    //PO_Total = po.PO_Total,
            //    //PO_OwnerID = id,
            //    //PO_ProductOrderStatusNo="1",
            //    //PO_ShipStatusNo="1",
            //    //PO_Receiver=po.PO_Receiver,
            //    //PO_RecipientAddress=po.PO_RecipientAddress,
            //    //PO_RecipientEmail=po.PO_RecipientEmail,
            //    //PO_RecipientPhone=po.PO_RecipientPhone,

            //};
            //    //string[] PO_ShipTypeNo = PO_ShipTypeNo[i];

            //    db.ProductOrder.Add(productOrder);

            //    for (int c = 0; c < ViewBag.pronum; c++)
            //    {
            //        ProductOrderDetails productOrderDetails = new ProductOrderDetails
            //        {
            //            PODetails_ProductOrderNo = ViewBag.pono,
            //            //PODetails_ProductOptionNo = pod.PODetails_ProductOptionNo,
            //            PODetails_Number = ViewBag.number,
            //            // PODetails_SubTotal = pod.PODetails_SubTotal,
            //        };
            //        db.ProductOrderDetails.Add(productOrderDetails);
            //    }

            //}


            //   db.SaveChanges();


            var newCar = db.Cart.Where(m => m.Cart_OwnerID == id).ToList();

            db.Cart.RemoveRange(newCar);

            db.SaveChanges();



            return RedirectToAction("ShowOrders", new { id = id });
        }

        private static string GetRandomStringByFileName()
         {
            var str = Path.GetRandomFileName().Replace(".", "");
            return str.Substring(0, 10);
         }

    }
}