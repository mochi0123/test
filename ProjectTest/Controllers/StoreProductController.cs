﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using ProjectTest.Models;

namespace ProjectTest.Controllers
{
    public class StoreProductController : Controller
    {
        Web5Entities db = new Web5Entities();
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyWeb5"].ConnectionString);

        [LoginRule]
        // GET: StoreProduct
        public ActionResult Index()
        {
            //if (Session["User_ID"] == null) return RedirectToAction("Login", "WebHome");

            string id = Session["User_ID"].ToString();
            var result = db.Product.Where(m => m.P_OwnerID == id);

            if(result.Count() > 0)
                ViewBag.status = result.Select(m => m.P_ProductStatusNo).First();


            //var r2 = db.Cart.Include("ProductOption").Where(m => m.Cart_OwnerID == id).Select(m => new { cartoption = m.Cart_ProductOptionNo, optionpic = m.ProductOption.POption_Pic, pname = m.ProductOption.Product.P_Name, seller = m.ProductOption.Product.User.User_StoreName });
            //var r3 = r2.ToList();
            //ViewBag.order = r3;
            //ViewBag.count = r2.Count();

            return View(result.ToList());
        }

        [LoginRule]
        public ActionResult Details(string no)
        {
            //if(Session["User_ID"] == null) return RedirectToAction("Login", "WebHome");

            var p_result = db.Product.Where(m => m.P_No == no).FirstOrDefault();
            var o_result = db.ProductOption.Where(m => m.POption_ProductNo == no);
            var sold = from d in db.ProductOrderDetails
                       group d by new { d.PODetails_ProductOptionNo } into g
                       select new { g.Key.PODetails_ProductOptionNo, sold_num = g.Sum(d => d.PODetails_Number) };
            //var r = from o in o_result
            //        join s in sold on o.POption_No equals s.PODetails_ProductOptionNo
            //        select o;

            var GroupJoin = o_result.GroupJoin(sold, o => o.POption_No, p => p.PODetails_ProductOptionNo, (o, c) => new { o, c }).SelectMany(o => o.c.DefaultIfEmpty(), (o, c) => new { no = o.o.POption_No, sum = (c == null ? 0 : c.sold_num) });
            Dictionary<string, int> temp = new Dictionary<string, int>();
            foreach (var item in GroupJoin) {
                temp.Add(item.no,item.sum);
            }
            ViewBag.sold_num = temp;

            StoreProductDetails sdp = new StoreProductDetails() {
                product = p_result,
                option = o_result.ToList()
            };

            return View(sdp);
        }

        public ActionResult Create() {
            if (Session["User_ID"] == null) return RedirectToAction("Login", "WebHome");

            CreateProduct cp = new CreateProduct() { 
                classes = db.ProductClass.ToList(),
                payTypes = db.PayType.ToList(),
                shipTypes = db.ShipType.ToList()
            };

            return View(cp);
        }

        
        [HttpPost]
        public ActionResult Create(string p_name,string p_intro, string p_class, HttpPostedFileBase cover, string format1, string format2, string[] option1, string[] option2, int[] price, int[] stock, string[] goods, HttpPostedFileBase[] p_pic, string[] pay, string[] ship,string status) {

            string coverName = "";
            if (cover != null)  //防止沒有選檔案就按上傳
            {
                if (cover.ContentLength > 0)  //防止空檔案上傳,ContentLength是檔案容量
                {
                    coverName = "COVER"+DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "").Replace("上午", "").Replace("下午", "") + ".jpg";
                    
                    cover.SaveAs(Server.MapPath("~/photo/product/" + coverName));
                }
            }

            string[] p_picName = new string[p_pic.Length];
            for (int i = 0; i < p_pic.Length; i++)
            {
                p_picName[i] = "";
                if (p_pic[i] != null)
                {
                    if (p_pic[i].ContentLength > 0)
                    {
                        p_picName[i] = "OPTION"+DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "").Replace("上午", "").Replace("下午", "") + (i + 1).ToString() + ".jpg";
                        
                        p_pic[i].SaveAs(Server.MapPath("~/Photo/product/" + p_picName[i]));
                    }
                }
            }

            int option_num = option1.Length;
            
            string[] option_no = new string[option_num];
            string last_option_no = db.ProductOption.OrderByDescending(m => m.POption_No).First().POption_No;
            int no = Int32.Parse(last_option_no.Substring(2, 8)) +1;
            option_no[0]= "PO" + no.ToString("D8");

            string insert_option = "insert into ProductOption values('" + option_no[0] + "','" + option1[0] + "', '" + option2[0] + "', '" + p_picName[0] + "', " + price[0] + ", " + stock[0] + ", '" + goods[0] + "', @p_no)";
            for (int i=1;i<option_num;i++) {
                option_no[i] = "PO" + (no + i).ToString("D8");
                insert_option += ",('" + option_no[i] + "','" + option1[i] + "', '" + option2[i] + "', '" + p_picName[i] + "', " + price[i] + ", " + stock[i] + ", '" + goods[i] + "', @p_no)";
            }

            string insert_pay = "insert into ProductPay values(@p_no,'" + pay[0] + "')";
            for (int i=1;i< pay.Length; i++) {
                insert_pay += ",(@p_no,'" + pay[i] + "')";
            }
            string insert_ship = "insert into ProductShip values(@p_no,'" + ship[0] + "')";
            for (int i = 1; i < ship.Length; i++)
            {
                insert_ship += ",(@p_no,'" + ship[i] + "')";
            }

            string sql="begin tran "+
                            "if  @f_no1 <> 'PF00000000' "+
                                "insert into ProductFormat values(@f_no1,@format1) "+
                            "if @@ERROR = 0 "+
                            "begin "+
                                "if  @f_no2 <> 'PF00000000' "+
                                    "insert into ProductFormat values(@f_no2,@format2) " +
			                    "if @@ERROR = 0 "+
                                "begin "+
                                    "insert into Product values(@p_no, @p_name, @p_intro, @cover, @f_no1, @f_no2, @status, @p_class, @User_ID) " +
                                    "if @@ERROR = 0 "+
                                    "begin " +insert_option+
                                        " if @@ERROR = 0 " +
                                        "begin " + insert_pay+
                                            " if @@ERROR = 0 " + 
                                            "begin "+ insert_ship +
                                                " if @@ERROR = 0 " +
                                                    "commit "+
                                                "else " +
                                                    "rollback " +
                                            "end " +
                                            "else " +
                                                "rollback " +
                                        "end " +
                                        "else " +
                                            "rollback " +
                                    "end " +
                                    "else "+
                                        "rollback "+
                                "end "+
                                "else "+
                                    "rollback "+
                            "end "+
                            "else "+
                                    "rollback";

            string p_no = db.Product.OrderByDescending(m => m.P_No).First().P_No;
            p_no = "P" + (Int32.Parse(p_no.Substring(1, 9)) + 1).ToString("D9");
            string f_no1 = db.ProductFormat.OrderByDescending(m=>m.PFormat_No).First().PFormat_No;
            f_no1 = "PF" + (Int32.Parse(f_no1.Substring(2, 8)) + 1).ToString("D8");
            string f_no2 = "PF" + (Int32.Parse(f_no1.Substring(2, 8)) + 1).ToString("D8");

            if (format1 == "") f_no1 = "PF00000000";
            if (format2 == "") f_no2 = "PF00000000";

            p_intro = p_intro.Replace("\n","<br>").Replace(" ", "&nbsp;");

            SqlCommand cmd = new SqlCommand(sql, Conn);
            cmd.Parameters.AddWithValue("@f_no1", f_no1);
            cmd.Parameters.AddWithValue("@format1", format1);
            cmd.Parameters.AddWithValue("@f_no2", f_no2);
            cmd.Parameters.AddWithValue("@format2", format2);
            cmd.Parameters.AddWithValue("@p_no", p_no);
            cmd.Parameters.AddWithValue("@p_name", p_name);
            cmd.Parameters.AddWithValue("@p_intro", p_intro);
            cmd.Parameters.AddWithValue("@cover", coverName);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@p_class", p_class);
            cmd.Parameters.AddWithValue("@User_ID", Session["User_ID"]);

            Conn.Open();
            cmd.ExecuteNonQuery();
            Conn.Close();

            return RedirectToAction("Index");
        }


        public string EditStatus(string p_status,string p_no) {
            string sql = "update [Product] set P_ProductStatusNo = @status where P_No=@p_no";

            SqlCommand cmd = new SqlCommand(sql, Conn);
            cmd.Parameters.AddWithValue("@status", p_status);
            cmd.Parameters.AddWithValue("@p_no", p_no);

            Conn.Open();
            cmd.ExecuteNonQuery();
            Conn.Close();

            return p_status+"//"+ p_no;
        }
    }
}