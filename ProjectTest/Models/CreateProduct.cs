﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectTest.Models;

namespace ProjectTest.Models
{
    public class CreateProduct
    {

        public List<ProductClass> classes { set; get; }

        public List<PayType> payTypes { set; get; }

        public List<ShipType> shipTypes { set; get; }
    }
}