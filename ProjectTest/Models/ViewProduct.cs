﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectTest.Models;

namespace ProjectTest.Models
{
    public class ViewProduct
    {
        public Product product { get; set; }

        public List<string> Option1 { get; set; }
        public List<string> Option2 { get; set; }

        public List<view_ProductShip> ShipFare { get; set; }
        public List<view_ProductPay> Pay { get; set; }

    }


    
}