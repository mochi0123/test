﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectTest.Models
{
    public class MetaUser
    {
        [DisplayName("會員ID")]
        public string User_ID { get; set; }
        [DisplayName("帳號")]
        [Required(ErrorMessage = "帳號為必填")]
        [StringLength(10, ErrorMessage = "帳號最多10個字")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "只能輸入英文或數字")]
        public string User_Account { get; set; }
        [DisplayName("密碼")]
        [Required(ErrorMessage = "密碼為必填")]
        [StringLength(10, ErrorMessage = "密碼最多10個字")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "只能輸入英文或數字")]
        public string User_Password { get; set; }
        [DisplayName("暱稱")]
        [Required(ErrorMessage = "暱稱為必填")]
        [StringLength(15, ErrorMessage = "暱稱最多15個字")]
        public string User_BuyerName { get; set; }
        [DisplayName("賣場名稱")]
        //[Required(ErrorMessage = "賣場名稱為必填")]
        //[StringLength(15, ErrorMessage = "賣場名稱最多15個字")]
        public string User_StoreName { get; set; }
        [DisplayName("真實姓名")]
        [Required(ErrorMessage = "真實姓名為必填")]
        [StringLength(15, ErrorMessage = "真實姓名最多15個字")]
        public string User_TrueName { get; set; }
        [DisplayName("信箱")]
        [Required(ErrorMessage = "信箱為必填")]
        [EmailAddress(ErrorMessage = "信箱格式有誤")]
        [StringLength(50, ErrorMessage = "信箱最多50個字")]
        [RegularExpression("^[a-zA-Z0-9@.]*$", ErrorMessage = "只能輸入英文或數字")]
        public string User_Email { get; set; }
        [DisplayName("手機")]
        [Required(ErrorMessage = "手機為必填")]
        [StringLength(15, ErrorMessage = "手機最多15個字")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "只能輸入數字")]
        public string User_Phone { get; set; }
        [DisplayName("性別")]
        [Required(ErrorMessage = "性別為必填")]
        public bool User_Gender { get; set; }
        [DisplayName("生日")]
        [Required(ErrorMessage = "生日為必填")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public System.DateTime User_Birth { get; set; }
        [DisplayName("大頭照")]
        public string User_Photo { get; set; }
        [DisplayName("地址")]
        public string User_Address { get; set; }
        public string User_StoreCoverPic { get; set; }
        public string User_StoreIntroduction { get; set; }
        public string User_StoreProductDisplay { get; set; }
        public string User_FbID { get; set; }
        [DisplayName("帳戶狀態")]
        public string User_Status { get; set; }

        public string User_StoreStatusNo { get; set; }
        public virtual StoreStatus StoreStatus { get; set; }
    }

    public class MetaProductOption
    {
        public string POption_No { get; set; }
        public string POption_Name1 { get; set; }
        public string POption_Name2 { get; set; }
        [DisplayName("商品圖片")]
        public string POption_Pic { get; set; }
        [DisplayName("單價")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:C0}")]
        public decimal POption_Price { get; set; }
        [DisplayName("庫存量")]
        public int POption_stock { get; set; }
        [DisplayName("商品貨號")]
        public string POption_GoodsNo { get; set; }
        public string POption_ProductNo { get; set; }
    }

    public class MetaProduct
    {
        public string P_No { get; set; }
        [DisplayName("商品名稱")]
        public string P_Name { get; set; }
        public string P_Details { get; set; }
        public string P_Pic { get; set; }
        public string P_ProductFormatNo1 { get; set; }
        public string P_ProductFormatNo2 { get; set; }

        public string P_ProductStatusNo { get; set; }

        public string P_ProductClassNo { get; set; }
        public string P_OwnerID { get; set; }
    }

    public class MetaProductClass
    {
        public string PClass_No { get; set; }
        [DisplayName("商品類別")]
        public string PClass_Name { get; set; }
    }
    public class MetaProductStatus
    {
        public string PS_No { get; set; }
        [DisplayName("商品狀態")]
        public string PS_Status { get; set; }
    }
    public class MetaComment
    {
        [Key]
        [DisplayName("留言編號")]
        [Required(ErrorMessage = "請輸入留言編號")]
        public int Comment_No { get; set; }
        [DisplayName("留言內容")]
        [Required(ErrorMessage = "請輸入留言內容")]
        public string Comment_Content { get; set; }
        [DisplayName("留言時間")]
        [Required(ErrorMessage = "請輸入留言時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public System.DateTime Comment_Date { get; set; }
        [DisplayName("檢舉狀態")]
        [Required(ErrorMessage = "請選擇被檢舉狀態")]
        public bool Comment_ReportStatus { get; set; }
        [DisplayName("商品編號")]
        [Required(ErrorMessage = "請輸商品編號")]
        public string Comment_ProductNo { get; set; }
        [DisplayName("會員ID")]
        [Required(ErrorMessage = "請輸入會員ID")]
        public string Comment_OwnerID { get; set; }
    }

    public class MetaProductOrder
    {
        [DisplayName("訂單編號")]
        public string PO_No { get; set; }
        [DisplayName("訂單成立日期")]
        public System.DateTime PO_CreateDate { get; set; }
        public string PO_PayTypeNo { get; set; }
        public string PO_ShipTypeNo { get; set; }
        [DisplayName("買家應付金額")]
        [DisplayFormat(DataFormatString = "{0:c0}")]
        public decimal PO_Total { get; set; }
        public string PO_OwnerID { get; set; }
        public string PO_ProductOrderStatusNo { get; set; }
        public string PO_ShipStatusNo { get; set; }
        public Nullable<System.DateTime> PO_CancelDate { get; set; }
        public string PO_Receiver { get; set; }
        public string PO_RecipientAddress { get; set; }
        public string PO_RecipientEmail { get; set; }
        public string PO_RecipientPhone { get; set; }
    }

    public class MetaShipType
    {
        public string ShipT_No { get; set; }
        [DisplayName("運送方式")]
        public string ShipT_Type { get; set; }
        public decimal ShipT_Fare { get; set; }
    }

    public class MetaShipStatus
    {
        public string ShipS_No { get; set; }
        [DisplayName("物流狀態")]
        public string ShipS_Status { get; set; }
    }

    public class MetaProductOrderDetails
    {
        public string PODetails_ProductOrderNo { get; set; }
        public string PODetails_ProductOptionNo { get; set; }
        [DisplayName("數量")]
        public int PODetails_Number { get; set; }
        [DisplayName("小計")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:C0}")]
        public Nullable<decimal> PODetails_SubTotal { get; set; }
    }
}