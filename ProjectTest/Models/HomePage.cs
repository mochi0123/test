﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectTest.Models;
using PagedList;

namespace ProjectTest.Models
{
    //首頁HomePage使用的ViewModel
    public class HomePage
    {
        public List<ProductClass> productclass { get; set; }
        public IPagedList<view_HomepageProduct> view_product { get; set; }
        public List<Ad> Ad { get; set; }

    }
}