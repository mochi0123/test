﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectTest.Models;

namespace ProjectTest.Models
{
    public class StoreProductDetails
    {
        public Product product { get; set; }
        public List<ProductOption> option { get; set; }

    }
}