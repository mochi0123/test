﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectTest.Models
{
    public class ViewCart
    {
    

        public List<PayType> payTypes  { get; set; }

        public List<ShipType> shipTypes { get; set; }

        public List<CreditCard> creditCards { get; set; }

        public List<BankAccount> bankAccounts { get; set; }

    }
}