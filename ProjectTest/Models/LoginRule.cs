﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectTest.Models
{
    public class LoginRule:ActionFilterAttribute
    {
        void LoginCheck(HttpContext context) {
            if (context.Session["User_ID"] == null) {
                context.Response.Redirect("/WebHome/HomePage");
            }
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //base.OnActionExecuting(filterContext);
            LoginCheck(HttpContext.Current);
        }

    }
}