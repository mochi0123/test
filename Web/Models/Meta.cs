﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
   
        public partial class MetaAd
        {
            [DisplayName("廣告編號")]
            public string Ad_No { get; set; }
            [DisplayName("廣告主題")]
            [Required(ErrorMessage = "請輸入廣告主題")]
            public string Ad_Name { get; set; }
            [DisplayName("圖片")]
            public string Ad_Pic { get; set; }
            [DisplayName("URL")]
            public string Ad_URL { get; set; }
            [DisplayName("圖片配置")]
            public string Ad_Layoutlocation { get; set; }
            [DisplayName("管理員ID")]
            public string Ad_AdminID { get; set; }
            
            public virtual Administrator Administrator { get; set; }
        }
    

    public class MetaAdministrator
    {
        [DisplayName("管理員ID")]        
        public string Admin_ID { get; set; }
        [DisplayName("管理員名稱")]
        [Required(ErrorMessage = "請輸入管理員名稱")]
        [StringLength(10, ErrorMessage = "管理員名稱最多輸入10個字")]
        public string Admin_Name { get; set; }
        [DisplayName("帳號")]
        [Required(ErrorMessage = "請輸入管理員帳號")]
        [StringLength(10, ErrorMessage = "管理員帳號最多輸入10個字")]
        public string Admin_Account { get; set; }
        [DisplayName("密碼")]
        [Required(ErrorMessage = "請輸入管理員密碼")]
        [StringLength(10, ErrorMessage = "管理員密碼最多輸入10個字")]
        public string Admin_Password { get; set; }
    }

    public class MetaBankAccount
    {
        [Key]
        [DisplayName("銀行帳號")]
        [Required(ErrorMessage = "請輸入銀行帳號")]
        public string BankAccount_Account { get; set; }
        [DisplayName("銀行名稱")]
        [Required(ErrorMessage = "請輸入銀行名稱")]
        [StringLength(3, ErrorMessage = "銀行名稱為3個字")]
        public string BankAccount_IssuerNo { get; set; }
        [DisplayName("會員ID")]
        [Required(ErrorMessage = "請輸入會員ID")]
        [StringLength(10, ErrorMessage = "會員ID為10個字")]
        public string BankAccount_OwnerID { get; set; }
    }

    public class MetaBlockRecord
    {
        [DisplayName("會員ID")]
        [Required(ErrorMessage = "請輸入被封鎖會員ID")]
        public string BlockR_UserID { get; set; }
        [DisplayName("審核員ID")]
        [Required(ErrorMessage = "請輸入管理員ID")]
        [StringLength(4, ErrorMessage = "管理員ID為4個字")]
        public string BlockR_AdminID { get; set; }
        [DisplayName("動作")]
        [Required(ErrorMessage = "請選擇狀態")]
        public bool BlockR_Action { get; set; }
        [DisplayName("時間")]
        [Required(ErrorMessage = "請輸入時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public System.DateTime BlockR_Date { get; set; }
        [DisplayName("封鎖原因")]
        [Required(ErrorMessage = "請輸入封鎖原因")]
        public string BlockR_Reason { get; set; }
    }

    public class MetaCart
    {
        [DisplayName("會員ID")]
        [Required(ErrorMessage = "請輸入會員ID")]
        public string Cart_OwnerID { get; set; }
        [DisplayName("商品規格選項")]
        [Required(ErrorMessage = "請輸入商品規格選項")]
        public string Cart_ProductOptionNo { get; set; }
        [DisplayName("商品數量")]
        [Required(ErrorMessage = "請輸入商品數量")]
        public int Cart_ProductNumber { get; set; }
        [DisplayName("展示狀態")]
        [Required(ErrorMessage = "請輸入展示狀態")]
        public bool Cart_ShowStatus { get; set; }
        [DisplayName("商品規格編號")]
        [Required(ErrorMessage = "請輸入商品規格編號")]
        public string Cart_ProductFormatNo { get; set; }
        [DisplayName("商品編號")]
        [Required(ErrorMessage = "請輸入商品編號")]
        public string Cart_ProductNo { get; set; }
    }

    public class MetaComment
    {
        [Key]
        [DisplayName("留言編號")]
        [Required(ErrorMessage = "請輸入留言編號")]
        public int Comment_No { get; set; }
        [DisplayName("留言內容")]
        [Required(ErrorMessage = "請輸入留言內容")]
        public string Comment_Content { get; set; }
        [DisplayName("留言時間")]
        [Required(ErrorMessage = "請輸入留言時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public System.DateTime Comment_Date { get; set; }
        [DisplayName("檢舉狀態")]
        [Required(ErrorMessage = "請選擇被檢舉狀態")]
        public bool Comment_ReportStatus { get; set; }
        [DisplayName("商品編號")]
        [Required(ErrorMessage = "請輸商品編號")]
        public string Comment_ProductNo { get; set; }
        [DisplayName("會員ID")]
        [Required(ErrorMessage = "請輸入會員ID")]
        public string Comment_OwnerID { get; set; }
    }

    public class MetaCreditCard
    {
        [Key]
        [DisplayName("信用卡號")]
        [Required(ErrorMessage = "請輸入信用卡號")]
        public string CreditCard_No { get; set; }
        [DisplayName("發卡行")]
        [Required(ErrorMessage = "請輸入發卡行")]
        [StringLength(3, ErrorMessage = "請輸入發卡銀行代碼3碼")]
        public string CreditCard_IssuerNo { get; set; }
        [DisplayName("會員ID")]
        [Required(ErrorMessage = "請輸入會員ID")]
        public string CreditCard_OwnerID { get; set; }
    }

    public class MetaIssuer
    {
        [Key]
        [DisplayName("銀行代號")]
        [Required(ErrorMessage = "請輸入銀行代號")]
        public string Issuer_No { get; set; }
        [DisplayName("銀行代號")]
        [Required(ErrorMessage = "請輸入銀行代號")]
        public string Issuer_Name { get; set; }
    }

    public class MetaPayType
    {
        [Key]
        [DisplayName("付款方式代號")]
        [Required(ErrorMessage = "請輸入付款方式代號")]
        public string PayT_No { get; set; }
        [DisplayName("付款方式")]
        [Required(ErrorMessage = "請輸入付款方式")]
        public string PayT_Type { get; set; }
    }

    public class MetaProduct
    {
        [Key]
        [DisplayName("商品編號")]
        [Required(ErrorMessage = "請輸入商品編號")]
        public string P_No { get; set; }
        [DisplayName("名稱")]
        [Required(ErrorMessage = "請輸入商品名稱")]
        [StringLength(50, ErrorMessage = "請輸入商品名稱最多50字")]
        public string P_Name { get; set; }
        [DisplayName("詳情")]
        [Required(ErrorMessage = "請輸入商品詳情")]
        public string P_Details { get; set; }
        [DisplayName("商品狀態分類")]
        [Required(ErrorMessage = "請輸入商品狀態分類")]
        public string P_ProductStatusNo { get; set; }
        [DisplayName("商品狀態分類")]
        [Required(ErrorMessage = "請輸入商品狀態分類")]
        public string P_ProductClassNo { get; set; }
        [DisplayName("所屬會員ID")]
        [Required(ErrorMessage = "請輸入會員ID")]
        public string P_OwnerID { get; set; }
    }

    public class MetaProductClass
    {
        [Key]
        [DisplayName("商品分類代號")]
        [StringLength(1, ErrorMessage = "商品分類代號最多1字")]
        [Required(ErrorMessage = "請輸入商品分類代號")]
        public string PClass_No { get; set; }
        [DisplayName("名稱")]
        [Required(ErrorMessage = "請輸入商品分類名稱")]
        [StringLength(10, ErrorMessage = "商品分類名稱最多10字")]
        public string PClass_Name { get; set; }
    }

    public class MetaProductFormat
    {
        [Key]
        [DisplayName("商品規格編號")]
        [Required(ErrorMessage = "請輸入商品規格編號")]
        public string PFormat_No { get; set; }
        [DisplayName("名稱")]
        [Required(ErrorMessage = "請輸入商品規格名稱")]
        [StringLength(20, ErrorMessage = "商品規格名稱最多敘述20字")]
        public string PFormat_Name { get; set; }
        [DisplayName("商品編號")]
        [Required(ErrorMessage = "請輸入商品編號")]
        public string PFormat_ProductNo { get; set; }
    }

    public class MetaProductOption
    {
        [Key]
        [DisplayName("商品規格選項編號")]
        [Required(ErrorMessage = "請輸入商品規格選項編號")]
        public string POption_No { get; set; }
        [DisplayName("名稱")]
        [Required(ErrorMessage = "請輸入商品規格選項名稱")]
        [StringLength(20, ErrorMessage = "商品規格選項名稱最多敘述20字")]
        public string POption_Name { get; set; }
        [DisplayName("圖片")]
        [Required(ErrorMessage = "請上傳商品規格圖片")]
        public string POption_Pic { get; set; }
        [DisplayName("單價")]
        [Required(ErrorMessage = "請輸入商品單價")]
        public decimal POption_Price { get; set; }
        [DisplayName("庫存量")]
        [Required(ErrorMessage = "請輸入商品庫存量")]
        public int POption_stock { get; set; }
        [DisplayName("選項貨號")]
        [StringLength(10, ErrorMessage = "貨號固定10字")]
        public string POption_GoodsNo { get; set; }
        [DisplayName("商品規格編號")]
        [Required(ErrorMessage = "請輸入商品規格編號")]
        public string POption_ProductFormatNo { get; set; }
    }

    public class MetaProductOrder
    {
        [Key]
        [DisplayName("商品訂單編號")]
        [Required(ErrorMessage = "請輸入商品訂單編號")]
        public string PO_No { get; set; }
        [DisplayName("成立時間")]
        [Required(ErrorMessage = "請輸入成立時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public System.DateTime PO_CreateDate { get; set; }
        [DisplayName("付款方式")]
        [Required(ErrorMessage = "請輸入付款方式")]
        public string PO_PayTypeNo { get; set; }
        [DisplayName("配送方式代號")]
        [Required(ErrorMessage = "請輸入配送方式代號")]
        public string PO_ShipTypeNo { get; set; }
        [DisplayName("總計")]
        [Required(ErrorMessage = "必填")]
        public decimal PO_Total { get; set; }
        [DisplayName("會員ID")]
        [Required(ErrorMessage = "請輸入會員ID")]
        public string PO_OwnerID { get; set; }
        [DisplayName("商品訂單狀態代號")]
        [Required(ErrorMessage = "請輸入商品訂單狀態代號")]
        public string PO_ProductOrderStatusNo { get; set; }
        [DisplayName("商品出貨狀態代號")]
        [Required(ErrorMessage = "請輸入商品出貨狀態代號")]
        public string PO_ShipStatusNo { get; set; }
        [DisplayName("取消時間")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> PO_CancelDate { get; set; }
        [DisplayName("收貨人")]
        [Required(ErrorMessage = "請輸入收貨人")]
        public string PO_Receiver { get; set; }
        [DisplayName("收貨地址")]
        [Required(ErrorMessage = "請輸入收貨地址")]
        [StringLength(50, ErrorMessage = "收貨地址最多50字")]
        public string PO_RecipientAddress { get; set; }
        [DisplayName("收貨人信箱")]
        [Required(ErrorMessage = "請輸入收貨人信箱")]
        [StringLength(50, ErrorMessage = "收貨人信箱最多50字")]
        public string PO_RecipientEmail { get; set; }
        [DisplayName("收貨人手機")]
        [Required(ErrorMessage = "請輸入收貨人手機")]
        public string PO_RecipientPhone { get; set; }
    }

    public class MetaProductOrderDetails
    {
        [Key]
        [DisplayName("商品訂單編號")]
        [Required(ErrorMessage = "請輸入商品訂單編號")]
        public string PODetails_ProductOrderNo { get; set; }
        [DisplayName("商品規格選項編號")]
        [Required(ErrorMessage = "請輸入商品規格選項編號")]
        public string PODetails_ProductOptionNo { get; set; }
        [DisplayName("數量")]
        [Required(ErrorMessage = "請輸入商品數量")]
        public int PODetails_Number { get; set; }
        [DisplayName("小計")]
        public Nullable<decimal> PODetails_SubTotal { get; set; }

    }

    public class MetaProductOrderStatus
    {
        [Key]
        [DisplayName("商品訂單狀態代號")]
        [Required(ErrorMessage = "請輸入商品訂單狀態代號")]
        public string POS_No { get; set; }
        [DisplayName("狀態敘述")]
        [Required(ErrorMessage = "請敘述商品訂單狀態")]
        [StringLength(20, ErrorMessage = "商品訂單狀態敘述最多20字")]
        public string POS_Status { get; set; }
    }

    public class MetaProductStatus
    {
        [Key]
        [DisplayName("商品狀態代號")]
        [Required(ErrorMessage = "請輸入商品狀態代號")]
        public string PS_No { get; set; }
        [DisplayName("狀態敘述")]
        [Required(ErrorMessage = "請敘述商品狀態")]
        [StringLength(5, ErrorMessage = "商品狀態敘述最多5字")]
        public string PS_Status { get; set; }
    }

    public class MetaShipStatus
    {
        [Key]
        [DisplayName("商品出貨狀態代號")]
        [Required(ErrorMessage = "請輸入商品出貨狀態代號")]
        public string ShipS_No { get; set; }
        [DisplayName("狀態敘述")]
        [Required(ErrorMessage = "請敘述商品出貨狀態")]
        [StringLength(10, ErrorMessage = "商品出貨狀態敘述最多10字")]
        public string ShipS_Status { get; set; }
    }

    public class MetaShipType
    {
        [Key]
        [DisplayName("運送方式代號")]
        [Required(ErrorMessage = "請輸入運送方式代號")]
        public string ShipT_No { get; set; }
        [DisplayName("方式")]
        [Required(ErrorMessage = "請輸入運送方式")]
        public string ShipT_Type { get; set; }
        [DisplayName("運費")]
        [Required(ErrorMessage = "請輸入運費")]
        public decimal ShipT_Fare { get; set; }
    }

    public class MetaStoreStatus
    {
        [Key]
        [DisplayName("賣場狀態代號")]
        [Required(ErrorMessage = "請輸入賣場狀態代號")]
        public string StoreS_No { get; set; }
        [DisplayName("狀態敘述")]
        [Required(ErrorMessage = "請敘述賣場狀態")]
        [StringLength(10, ErrorMessage = "賣場狀態敘述最多10字")]
        public string StoreS_Status { get; set; }
    }

    public class MetaUser
    {
        [DisplayName("會員ID")]
        [Required(ErrorMessage = "請輸入會員ID")]
        public string User_ID { get; set; }
        [DisplayName("帳號")]
        [Required(ErrorMessage = "請輸入帳號")]
        [StringLength(10, ErrorMessage = "帳號最多輸入10字")]
        public string User_Account { get; set; }
        [DisplayName("密碼")]
        [Required(ErrorMessage = "請輸入密碼")]
        [StringLength(10, ErrorMessage = "密碼最多輸入10字")]
        public string User_Password { get; set; }
        [DisplayName("買方名稱")]
        [Required(ErrorMessage = "請輸入買方名稱")]
        [StringLength(15, ErrorMessage = "買方名稱最多輸入15字")]
        public string User_BuyerName { get; set; }
        [DisplayName("賣場名稱")]
        [Required(ErrorMessage = "請輸入賣場名稱")]
        [StringLength(15, ErrorMessage = "賣場名稱最多輸入15字")]
        public string User_StoreName { get; set; }
        [DisplayName("真實性名")]
        [Required(ErrorMessage = "請輸入真實姓名")]
        [StringLength(15, ErrorMessage = "姓名最多輸入15字")]
        public string User_TrueName { get; set; }
        [DisplayName("信箱")]
        [Required(ErrorMessage = "請輸入電子信箱")]
        [StringLength(50, ErrorMessage = "電子信箱最多輸入50字")]
        public string User_Email { get; set; }
        [DisplayName("手機")]
        [Required(ErrorMessage = "請輸入手機")]
        [StringLength(15, ErrorMessage = "手機最多輸入15字")]
        public string User_Phone { get; set; }
        [DisplayName("性別")]
        [Required(ErrorMessage = "必填")]
        public bool User_Gender { get; set; }
        [DisplayName("生日")]
        [Required(ErrorMessage = "請輸入生日")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public System.DateTime User_Birth { get; set; }
        [DisplayName("大頭照")]
        [Required(ErrorMessage = "請上傳照片")]
        public string User_Photo { get; set; }
        [DisplayName("地址")]
        [Required(ErrorMessage = "請輸入地址")]
        [StringLength(50, ErrorMessage = "地址最多輸入50字")]
        public string User_Address { get; set; }
        [DisplayName("賣場封面圖")]
        [Required(ErrorMessage = "請上傳照片")]
        public string User_StoreCoverPic { get; set; }
        [DisplayName("賣場介紹文案")]
        [Required(ErrorMessage = "必填")]
        public string User_StoreIntroduction { get; set; }
        [DisplayName("賣場商品展示圖")]
        [Required(ErrorMessage = "請上傳照片")]
        public string User_StoreProductDisplay { get; set; }
        [DisplayName("FB帳號ID")]
        [Required(ErrorMessage = "請輸入FB帳號")]
        [StringLength(20, ErrorMessage = "FB帳號最多輸入20字")]
        public string User_FbID { get; set; }
        [DisplayName("會員帳戶狀態")]
        public string User_Status { get; set; }
        [DisplayName("賣場狀態代號")]
        public string User_StoreStatusNo { get; set; }
    }
}