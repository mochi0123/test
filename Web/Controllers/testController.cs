﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using PagedList;


namespace Web.Controllers
{
    public class testController : Controller
    {
        Web5Entities db = new Web5Entities();
        int pageSize = 10;
        // GET: test
        public ActionResult Createad()
        {
            if (Session["Adm"] == null)
            {
                return RedirectToAction("Login","Home");
            }
            return View("Createad", "_LayoutAdministrator");
        }
        [HttpPost]
        public ActionResult Createad(HttpPostedFileBase photo, Ad ad)
        {
            if (Session["Adm"] == null)
            {
                return RedirectToAction("Login", "Home");
            }

            //取得ID            
            if (getAd_No() <= 99999999)
                ad.Ad_No = "AD" + getAd_No().ToString("D8");

            else
            {
                ViewBag.error += "廣告已達上限!<br>";
                //資料輸入錯誤，回到註冊頁面
                return View(ad);
            }

            //取得管理員
            ad.Ad_AdminID = Session["Adm"].ToString();

            //取得圖片
            if (photo != null)
            {
                if (photo.ContentLength > 0)
                {
                    string FileName = "";
                    FileName = photo.FileName;
                    FileName = Path.GetFileName(FileName);
                    FileName = DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "").Replace("上午", "").Replace("下午", "") + ".jpg";
                    photo.SaveAs(Server.MapPath("~/Photos/" + FileName));
                    ad.Ad_Pic = FileName;
                }
            } 
            else
            {
                ad.Ad_Pic = "p0.jpg";
            }

            

            if (ModelState.IsValid)
            {
                db.Ad.Add(ad);
                db.SaveChanges();
                return RedirectToAction("Index", "test");
            }
            
            if (Session["Adm"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View("Createad", "_LayoutAdministrator");
        }
        public int getAd_No()//取得可用ID
        {
            int i = 0;
            string id = "";
            var result = db.Ad.OrderByDescending(m => m.Ad_No).Take(1);
            var re=db.Ad.Where(m => m.Ad_No == "AD00000001").FirstOrDefault();
            if (re != null) 
            {
                foreach (var r in result)
                    id = r.Ad_No;
                id = id.ToString().Substring(2, 8);
                i = Int32.Parse(id) + 1;
                Console.WriteLine(id); 
            }
            else 
            { 
            i = 1;
            }
            return i;
        }
        public ActionResult editAD(string id)
        {

            var ad = db.Ad.Where(m => m.Ad_No == id).FirstOrDefault();
            if (Session["Adm"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View("editAD", "_LayoutAdministrator", ad);
        }
        [HttpPost]
        public ActionResult editAD(Ad a, HttpPostedFileBase photo)
        {
            if (Session["Adm"] == null)
            {
                return RedirectToAction("Login", "Home");
            }

            var EaD = db.Ad.Where
                 (m => m.Ad_No == a.Ad_No).FirstOrDefault();
            
            if (photo != null)
            {
                if (photo.ContentLength > 0)
                {
                    string FileName = "";
                    FileName = photo.FileName;
                    FileName = Path.GetFileName(FileName);
                    FileName = DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "").Replace("上午", "").Replace("下午", "") + ".jpg";
                    photo.SaveAs(Server.MapPath("~/Photos/" + FileName));
                    EaD.Ad_Pic = FileName;
                }
            }
            EaD.Ad_URL = a.Ad_URL;
            EaD.Ad_Name = a.Ad_Name;
            EaD.Ad_Layoutlocation = a.Ad_Layoutlocation;

            if (ModelState.IsValid)
            {
                db.SaveChanges();
                return RedirectToAction("Index", "test");
            }

            if (Session["Adm"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View("editAD", "_LayoutAdministrator");
        }
        public ActionResult Index(int page = 1)
        {
            int currentPage = page < 1 ? 1 : page;
            var ads = db.Ad.ToList();
            var result = ads.ToPagedList(currentPage, pageSize);
            if (Session["Adm"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View("Index", "_LayoutAdministrator", result);
        }

        //-刪除
        public ActionResult Delete(string id)
        {
            var de = db.Ad.Where(m => m.Ad_No == id).FirstOrDefault();
            db.Ad.Remove(de);
            db.SaveChanges();
            return RedirectToAction("Index");

        }
        //-詳情
        public ActionResult Details(string id) //廣告詳細內容
        {

            var deli=db.Ad.Where(m => m.Ad_No == id).ToList();

            if (Session["Adm"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View("Details", "_LayoutAdministrator", deli);

        }
        
        
    }
}