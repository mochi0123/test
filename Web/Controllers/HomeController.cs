﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using System.Data.SqlClient;
using System.Configuration;
using PagedList;



namespace Web.Controllers
{
    public class HomeController : Controller
    {
        Web5Entities db = new Web5Entities();
        int pageSize = 10;
        
        // GET: Home

        public ActionResult Index(int page = 1)//會員管理介面
        {
            int currentPage = page < 1 ? 1 : page;
            var users = db.User.ToList();
            var result = users.ToPagedList(currentPage, pageSize);
            if (Session["Adm"] == null)
            {
                return View("Login", "_Layout");
            }
            return View("Index", "_LayoutAdministrator", result);
        }
            
            
            
        public ActionResult Login()//管理者登入
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(string Admin_Account, string Admin_Password)//管理者登入
        {
            var Adm = db.Administrator
                .Where(m => m.Admin_Account == Admin_Account && m.Admin_Password == Admin_Password)
                .FirstOrDefault();
            if (Adm == null)
            {
                ViewBag.Message = "帳密錯誤,登入失敗";
                return View();
            }
            Session["Name"]=db.Administrator.Where(m => m.Admin_Account == Admin_Account && m.Admin_Password == Admin_Password)
                .FirstOrDefault().Admin_Name;


            Session["Adm"] = db.Administrator
                .Where(m => m.Admin_Account == Admin_Account && m.Admin_Password == Admin_Password)
                .FirstOrDefault().Admin_ID;
            return RedirectToAction("Homepage");
        }
        public ActionResult ManagerList(int page = 1)//管理者清單
        {
            int currentPage = page < 1 ? 1 : page;
            var Admn = db.Administrator.ToList();
            var result = Admn.ToPagedList(currentPage, pageSize);
            if (Session["Adm"] == null)
            {
                return View("Login", "_Layout", result);
            }
            return View("ManagerList", "_LayoutAdministrator", result);
        }
        public ActionResult Register()//管理者註冊
        {
           
            return View();
        }
        [HttpPost]
        public ActionResult Register(Administrator u, string Password)//管理者註冊
        {
            
            if (getAdminID() <= 999)
                u.Admin_ID = "A" + getAdminID().ToString("D3");
            else
            {
                ViewBag.error += "會員已達上限!<br>";
                //資料輸入錯誤，回到註冊頁面
                return View(u);
            }
            //確認密碼
            if (u.Admin_Password != Password)
            {
                ViewBag.checkpwd = "與密碼不相符!";
                //資料輸入錯誤，回到註冊頁面
                return View(u);
            }

            if (ModelState.IsValid == false)
            {
                return View();
            }
            var Admin = db.Administrator
                 .Where(m => m.Admin_ID == u.Admin_ID||m.Admin_Account== u.Admin_Account)
                 .FirstOrDefault();
             if (Admin == null)
            {
                db.Administrator.Add(u);
                db.SaveChanges();
                ViewBag.Message = "註冊成功!!";
                //return RedirectToAction("Register");
                return View();

            }
            ViewBag.Message = "此帳號已有人使用，註冊失敗!!";
            return View();
             
                 
        }
        public int getAdminID()//取得可用ID
        {
            int i = 0;
            string id = "";
            var result = db.Administrator.OrderByDescending(m => m.Admin_ID).Take(1);
            var re = db.Administrator.Where(m => m.Admin_ID == "A001").FirstOrDefault();
            if (re != null)
            {
            foreach (var r in result)
            id = r.Admin_ID;
            id = id.ToString().Substring(1, 3);
            i = Int32.Parse(id) + 1;
            //id = "U" + i.ToString("D9");
            Console.WriteLine(id);
            }
            else
            {
                i = 1;
            }
            return i;
        }


        public ActionResult Logout()//(登出)
        {
            Session["Adm"] = null;
            //Session.Clear();
            return RedirectToAction("Login");
        }

        
        public ActionResult Edit(string id)//凍結、解鎖帳號(喚出表單)
        {
            
            var users = db.User.Where
                (m => m.User_ID == id).FirstOrDefault();         
            if (Session["Adm"] == null)
            {
                return View("Login", "_Layout", users);
            }
            return View("Edit", "_LayoutAdministrator", users);
            
        }

        
        [HttpPost]
        public ActionResult Edit(string User_ID, string User_Status)//凍結、解鎖帳號(儲存)
        {
            if (Session["Adm"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebConnectionString"].ConnectionString);
            SqlCommand Cmd = new SqlCommand("update [User] set User_Status=@User_Status where User_ID=@User_ID", Conn);
            Cmd.Parameters.AddWithValue("@User_Status", User_Status);
            Cmd.Parameters.AddWithValue("@User_ID", User_ID);

            Conn.Open();

            Cmd.ExecuteNonQuery();

            Conn.Close();

            return RedirectToAction("Index");
           
        }
        public ActionResult Delete(string id)//刪除管理員
        {
            var AdId = db.Administrator.Where(m => m.Admin_ID == id).FirstOrDefault();

            db.Administrator.Remove(AdId);
            db.SaveChanges();

            return RedirectToAction("ManagerList");
        }
       public ActionResult PrdClass(int page = 1)//種類清單
        {
            int currentPage = page < 1 ? 1 : page;
            var Prd= db.ProductClass.ToList();
            var result = Prd.ToPagedList(currentPage, pageSize);
            if (Session["Adm"] == null)
            {
                return View("Login", "_Layout");
            }
            return View("PrdClass", "_LayoutAdministrator", result);
            //return View(Prd);
        }
        public ActionResult PrdDelete(string id)//刪除產品類別
        {
            var PdId = db.ProductClass.Where(m => m.PClass_No == id).FirstOrDefault();

            db.ProductClass.Remove(PdId);
            db.SaveChanges();

            return RedirectToAction("PrdClass");
        }
        public ActionResult PrdCreat()//新增產品類別
        {
          
            if (Session["Adm"] == null)
            {
                return View("Login", "_Layout");
            }
            return View("PrdCreat", "_LayoutAdministrator");
          
        }
        [HttpPost]
        public ActionResult PrdCreat(ProductClass Pdc)//新增產品類別
        {

            if (Session["Adm"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (ModelState.IsValid == false)
            {
                return View();
            }
            var PDCId = db.ProductClass
                 .Where(m => m.PClass_No == Pdc.PClass_No||m.PClass_Name==Pdc.PClass_Name )
                 .FirstOrDefault();           
            if (PDCId == null)
            {
                db.ProductClass.Add(Pdc);
                db.SaveChanges();
                return RedirectToAction("PrdClass");
            }
            ViewBag.Message = "此商品分類代號或名稱已使用，新增失敗!!";
           
            if (Session["Adm"] == null)
            {
                return View("Login", "_Layout");
            }
            return View("PrdCreat", "_LayoutAdministrator");           
        }
        public ActionResult Homepage()
        {
            if (Session["Adm"] == null)
            {
                return View("Login", "_Layout");
            }
            return View("Homepage", "_LayoutAdministrator");            
        }
        public ActionResult Find()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Find(Administrator ads)
        {
            var ad = db.Administrator.Where(m => m.Admin_Account == ads.Admin_Account).FirstOrDefault();
            if (ad == null)
            {
                ViewBag.mess = "查無此帳號!";
                return View();
            }
            else
            {
                TempData["AdmAcc"] = ad.Admin_Account;
            }
            ViewBag.ad = ad;

            //return View();
            return RedirectToAction("IndexFind", "Home");
        }

        public ActionResult IndexFind()
        {
            var ad = TempData["AdmAcc"].ToString();
            //var ad = "ABC123";
            var adf = db.Administrator.Where(m => m.Admin_Account == ad).ToList();

            return View(adf);
        }













    }
}
